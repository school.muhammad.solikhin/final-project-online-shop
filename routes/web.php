<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Pembeli
Route::get('/', 'pembeli@index');


/*
Route::get('/admin', function () {
    return view('layouts.master');
});
*/
//Login
Route::get('/login', function () {
    return view('login');
});

Route::post('/login', function () {
    return view('login');
});

//Register
Route::get('/register', function () {
    return view('register');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('barang', 'BarangController');
Route::resource('seller', 'SellerController');
Route::resource('product', 'ProductController');
Route::resource('kategori', 'CategoryController');

// Route::get('/order/create/{id}', 'OrderController@create');

Route::get('/order/create', 'OrderController@create');
Route::post('/order', 'OrderController@store');
Route::get('/order', 'OrderController@index');
Route::get('/order/{id}', 'OrderController@show');
Route::get('/order/{id}/edit', 'OrderController@edit');
Route::put('/order/{id}', 'OrderController@update');
Route::delete('/order/{id}', 'OrderController@destroy');   

Route::group(['middleware' => ['auth']], function() {
    Route::get('/cart', 'pembeli@cart');
    Route::get('/checkout', 'pembeli@checkout');

    Route::get('/kategori/laporan', 'CategoryController@laporan');
    Route::get('/kategori/pdf', 'CategoryController@pdf');
    Route::get('/kategori/export', 'CategoryController@export');

    Route::get('/seller/laporan', 'SellerController@laporan');
    Route::get('/seller/pdf', 'SellerController@pdf');
    Route::get('/seller/export', 'SellerController@export');

    Route::resource('product', 'ProductController');
    Route::resource('kategori', 'CategoryController');    
    Route::resource('seller', 'SellerController');
    Route::resource('barang', 'BarangController');
});

    Route::resource('profile', 'ProfileController');
    Route::resource('profile', 'ControllerProfile');

