<nav class="navbar navbar-fixed-top" id="navigation">
    <!-- container -->
    <div class="container">
      <!-- responsive-nav -->
      <div id="responsive-nav">
        <!-- NAV -->
        <ul class="main-nav nav navbar-nav">
          <li class="active"><a href="/">Home</a></li>
          <li><a href="/register">Register</a></li>
          <li><a href="/login">Login</a></li>
        </ul>
        <!-- /NAV -->
      </div>
      <!-- /responsive-nav -->
      
    </div>
    
    <!-- /container -->
  </nav>
  