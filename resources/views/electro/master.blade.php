<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <title>Ecommerce</title>

    <!-- Google font -->
    <link href="{{ secure_asset('/electro') }}/https://fonts.googleapis.com/css?family=Montserrat:400,500,700"
        rel="stylesheet" />

    <!-- Bootstrap -->
    <link type="text/css" rel="stylesheet" href="{{ secure_asset('/electro') }}/css/bootstrap.min.css" />

    <!-- Slick -->
    <link type="text/css" rel="stylesheet" href="{{ secure_asset('/electro') }}/css/slick.css" />
    <link type="text/css" rel="stylesheet" href="{{ secure_asset('/electro') }}/css/slick-theme.css" />

    <!-- nouislider -->
    <link type="text/css" rel="stylesheet" href="{{ secure_asset('/electro') }}/css/nouislider.min.css" />

    <!-- Font Awesome Icon -->
    <link rel="stylesheet" href="{{ secure_asset('/electro') }}/css/font-awesome.min.css" />

    <!-- Custom stlylesheet -->
    <link type="text/css" rel="stylesheet" href="{{ secure_asset('/electro') }}/css/style.css" />

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="{{ secure_asset('/electro') }}/https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="{{ secure_asset('/electro') }}/https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <!-- HEADER -->
    @include('electro.partial.header')
    <!-- /HEADER -->

    <!-- NAVIGATION -->
    @include('electro.partial.navbar')
    <!-- /NAVIGATION -->
    <!-- SECTION -->

    <!-- /SECTION -->
    <!-- div class="section">
        <div class="container">
            <div class="row fullscreen align-items-center justify-content-center" style="height: 634px">
                <div class="col-lg-6 col-md-12 d-flex align-self-end img-right no-padding">
                    <img id="img" class="img-fluid" src="{{secure_asset('img/test.jpg')}}" alt="">
                </div>
                <div class="banner-content col-lg-6 col-md-12">
                    <h1 class="title-top">
                        <span>Flat</span>
                        75% Off
                    </h1>
                    <h1 class="text-uppercase">
                        It's Happening
                        <br>
                        This Season
                    </h1>
                    <button class="primary-btn text-uppercase">
                        <a style="color: white" href="">Purchase Now</a>
                    </button>
                </div>
            </div>
        </div>
    </div-->
    <!-- SECTION -->
    <div class="section">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <!-- shop -->
                @forelse ($Product as $key=>$value)
                <div class="col-md-4 col-xs-6">
                    <div class="shop">
                        <div class="shop-img">
                            <img src="{{secure_asset('upload/product/'.$value->gambar)}}" alt="Product">
                        </div>
                        <div class="shop-body">
                            <h3>{{$value->nama}}<br />Collection</h3>
                            <a href="/order/{{$value->id}}" class="cta-btn">
                                Shop now <i class="fa fa-arrow-circle-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
                @empty
                <div class="col-md-4 col-xs-6">
                    <h3>No Data</h3>
                </div>
                @endforelse
                <!-- /shop -->
            </div>
            <!-- /row -->
        </div>
        <!-- /container -->
    </div>
    <!-- /SECTION -->

    <!-- SECTION -->
    <div class="section">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <!-- section title -->
                <div class="col-md-12">
                    <div class="section-title">
                        <h3 class="title">Products</h3>
                        <div class="section-nav">
                            <ul class="section-tab-nav tab-nav">
                                <li class="active">
                                    <a data-toggle="tab" href="#/tab1">Laptops</a>
                                </li>
                                <li><a data-toggle="tab" href="/#tab1">Smartphones</a></li>
                                <li><a data-toggle="tab" href="/#tab1">Cameras</a></li>
                                <li><a data-toggle="tab" href="/#tab1">Accessories</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- /section title -->

                <!-- Products tab & slick -->
                <div class="col-md-12">
                    <div class="row">
                        <div class="products-tabs">
                            <!-- tab -->
                            <div id="tab1" class="tab-pane active">
                                <div class="products-slick" data-nav="#slick-nav-1">
                                    <!-- product -->
                                    <?php
                                        function rp($angka) {
                                                    $rupiah = "Rp " . number_format($angka,0,',','.');
                                                    return $rupiah;
                                                    }
                                    ?>
                                    @forelse ($join as $key=>$value)
                                    <div class="product">
                                        <div class="product-img">
                                            <img src="{{secure_asset('upload/product/'.$value->gambar)}}" alt="Product">
                                        </div>
                                        <div class="product-body">
                                            <p class="product-category">{{$value->nama_kategori}}</p>
                                            <h3 class="product-name">
                                                <a href="{{ secure_asset('/electro') }}/#">{{$value->nama}}</a>
                                            </h3>
                                            <h4 class="product-price">
                                                <?php 
                                                
                                                echo rp($value->harga)
                                                ?>
                                            </h4>
                                            <div class="product-rating">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </div>
                                            <div class="product-btns">
                                                <button class="add-to-wishlist">
                                                    <i class="fa fa-heart-o"></i><span class="tooltipp">add to
                                                        wishlist</span>
                                                </button>
                                                <button class="add-to-compare">
                                                    <i class="fa fa-exchange"></i><span class="tooltipp">add to
                                                        compare</span>
                                                </button>
                                                <button class="quick-view">
                                                    <i class="fa fa-eye"></i><span class="tooltipp">quick view</span>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="add-to-cart">
                                         <a href="/order/create" class="btn btn-info btn-sm">GO TO CART</a>
                                        </div>
                                    </div>
                                    @empty
                                    <div class="col-md-4 col-xs-6">
                                        <h3>No Data</h3>
                                    </div>
                                    @endforelse
                                    <!-- /product -->
                                </div>
                                <div id="slick-nav-1" class="products-slick-nav"></div>
                            </div>
                            <!-- /tab -->
                        </div>
                    </div>
                </div>
                <!-- Products tab & slick -->
            </div>
            <!-- /row -->
        </div>
        <!-- /container -->
    </div>
    <!-- /SECTION -->

    <!-- HOT DEAL SECTION -->
    <div id="hot-deal" class="section">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <div class="col-md-12">
                    <div class="hot-deal">
                        <ul class="hot-deal-countdown">
                            <li>
                                <div>
                                    <h3>02</h3>
                                    <span>Days</span>
                                </div>
                            </li>
                            <li>
                                <div>
                                    <h3>10</h3>
                                    <span>Hours</span>
                                </div>
                            </li>
                            <li>
                                <div>
                                    <h3>34</h3>
                                    <span>Mins</span>
                                </div>
                            </li>
                            <li>
                                <div>
                                    <h3>60</h3>
                                    <span>Secs</span>
                                </div>
                            </li>
                        </ul>
                        <h2 class="text-uppercase">hot deal this week</h2>
                        <p>New Collection Up to 50% OFF</p>
                    </div>
                </div>
            </div>
            <!-- /row -->
        </div>
        <!-- /container -->
    </div>
    <!-- /HOT DEAL SECTION -->

    <!-- SECTION -->
    <div class="section">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <!-- section title -->
                <div class="col-md-12">
                    <div class="section-title">
                        <h3 class="title">Top selling</h3>
                        <div class="section-nav">
                            <ul class="section-tab-nav tab-nav">
                                <li class="active">
                                    <a data-toggle="tab" href="{{ secure_asset('/electro') }}/#tab2">Laptops</a>
                                </li>
                                <li><a data-toggle="tab" href="{{ secure_asset('/electro') }}/#tab2">Smartphones</a></li>
                                <li><a data-toggle="tab" href="{{ secure_asset('/electro') }}/#tab2">Cameras</a></li>
                                <li><a data-toggle="tab" href="{{ secure_asset('/electro') }}/#tab2">Accessories</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- /section title -->

                <!-- Products tab & slick -->
                <div class="col-md-12">
                    <div class="row">
                        <div class="products-tabs">
                            <!-- tab -->
                            <div id="tab2" class="tab-pane fade in active">
                                <div class="products-slick" data-nav="#slick-nav-2">
                                    <!-- product -->
                                    @forelse ($join as $key=>$value)
                                    <div class="product">
                                        <div class="product-img">
                                            <img src="{{secure_asset('upload/product/'.$value->gambar)}}" alt="Product">
                                        </div>
                                        <div class="product-body">
                                            <p class="product-category">{{$value->nama_kategori}}</p>
                                            <h3 class="product-name">
                                                <a href="{{ secure_asset('/electro') }}/#">{{$value->nama}}</a>
                                            </h3>
                                            <h4 class="product-price">
                                                <?php 
                                                
                                                echo rp($value->harga)
                                                ?>
                                            </h4>
                                            <div class="product-rating">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </div>
                                            <div class="product-btns">
                                                <button class="add-to-wishlist">
                                                    <i class="fa fa-heart-o"></i><span class="tooltipp">add to
                                                        wishlist</span>
                                                </button>
                                                <button class="add-to-compare">
                                                    <i class="fa fa-exchange"></i><span class="tooltipp">add to
                                                        compare</span>
                                                </button>
                                                <button class="quick-view">
                                                    <i class="fa fa-eye"></i><span class="tooltipp">quick view</span>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="add-to-cart">
                                        <a href="/order/create" class="btn btn-info btn-sm">GO TO CART</a>
                                        </div>
                                    </div>
                                    @empty
                                    <div class="col-md-4 col-xs-6">
                                        <h3>No Data</h3>
                                    </div>
                                    @endforelse
                                </div>
                                <div id="slick-nav-2" class="products-slick-nav"></div>
                            </div>
                            <!-- /tab -->
                        </div>
                    </div>
                </div>
                <!-- /Products tab & slick -->
            </div>
            <!-- /row -->
        </div>
        <!-- /container -->
    </div>
    <!-- /SECTION -->

    <!-- SECTION -->
    <div class="section">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <div class="col-md-4 col-xs-6">
                    <div class="section-title">
                        <h4 class="title">Top selling</h4>
                        <div class="section-nav">
                            <div id="slick-nav-3" class="products-slick-nav"></div>
                        </div>
                    </div>

                    <div class="products-widget-slick" data-nav="#slick-nav-3">
                        <div>
                            <!-- product widget -->
                            <div class="product-widget">
                                <div class="product-img">
                                    <img src="{{ secure_asset('/electro') }}/img/product07.png" alt="" />
                                </div>
                                <div class="product-body">
                                    <p class="product-category">Category</p>
                                    <h3 class="product-name">
                                        <a href="{{ secure_asset('/electro') }}/#">product name goes here</a>
                                    </h3>
                                    <h4 class="product-price">
                                        $980.00 <del class="product-old-price">$990.00</del>
                                    </h4>
                                </div>
                            </div>
                            <!-- /product widget -->

                            <!-- product widget -->
                            <div class="product-widget">
                                <div class="product-img">
                                    <img src="{{ secure_asset('/electro') }}/img/product08.png" alt="" />
                                </div>
                                <div class="product-body">
                                    <p class="product-category">Category</p>
                                    <h3 class="product-name">
                                        <a href="{{ secure_asset('/electro') }}/#">product name goes here</a>
                                    </h3>
                                    <h4 class="product-price">
                                        $980.00 <del class="product-old-price">$990.00</del>
                                    </h4>
                                </div>
                            </div>
                            <!-- /product widget -->

                            <!-- product widget -->
                            <div class="product-widget">
                                <div class="product-img">
                                    <img src="{{ secure_asset('/electro') }}/img/product09.png" alt="" />
                                </div>
                                <div class="product-body">
                                    <p class="product-category">Category</p>
                                    <h3 class="product-name">
                                        <a href="{{ secure_asset('/electro') }}/#">product name goes here</a>
                                    </h3>
                                    <h4 class="product-price">
                                        $980.00 <del class="product-old-price">$990.00</del>
                                    </h4>
                                </div>
                            </div>
                            <!-- product widget -->
                        </div>

                        <div>
                            <!-- product widget -->
                            <div class="product-widget">
                                <div class="product-img">
                                    <img src="{{ secure_asset('/electro') }}/img/product01.png" alt="" />
                                </div>
                                <div class="product-body">
                                    <p class="product-category">Category</p>
                                    <h3 class="product-name">
                                        <a href="{{ secure_asset('/electro') }}/#">product name goes here</a>
                                    </h3>
                                    <h4 class="product-price">
                                        $980.00 <del class="product-old-price">$990.00</del>
                                    </h4>
                                </div>
                            </div>
                            <!-- /product widget -->

                            <!-- product widget -->
                            <div class="product-widget">
                                <div class="product-img">
                                    <img src="{{ secure_asset('/electro') }}/img/product02.png" alt="" />
                                </div>
                                <div class="product-body">
                                    <p class="product-category">Category</p>
                                    <h3 class="product-name">
                                        <a href="{{ secure_asset('/electro') }}/#">product name goes here</a>
                                    </h3>
                                    <h4 class="product-price">
                                        $980.00 <del class="product-old-price">$990.00</del>
                                    </h4>
                                </div>
                            </div>
                            <!-- /product widget -->

                            <!-- product widget -->
                            <div class="product-widget">
                                <div class="product-img">
                                    <img src="{{ secure_asset('/electro') }}/img/product03.png" alt="" />
                                </div>
                                <div class="product-body">
                                    <p class="product-category">Category</p>
                                    <h3 class="product-name">
                                        <a href="{{ secure_asset('/electro') }}/#">product name goes here</a>
                                    </h3>
                                    <h4 class="product-price">
                                        $980.00 <del class="product-old-price">$990.00</del>
                                    </h4>
                                </div>
                            </div>
                            <!-- product widget -->
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-xs-6">
                    <div class="section-title">
                        <h4 class="title">Top selling</h4>
                        <div class="section-nav">
                            <div id="slick-nav-4" class="products-slick-nav"></div>
                        </div>
                    </div>

                    <div class="products-widget-slick" data-nav="#slick-nav-4">
                        <div>
                            <!-- product widget -->
                            <div class="product-widget">
                                <div class="product-img">
                                    <img src="{{ secure_asset('/electro') }}/img/product04.png" alt="" />
                                </div>
                                <div class="product-body">
                                    <p class="product-category">Category</p>
                                    <h3 class="product-name">
                                        <a href="{{ secure_asset('/electro') }}/#">product name goes here</a>
                                    </h3>
                                    <h4 class="product-price">
                                        $980.00 <del class="product-old-price">$990.00</del>
                                    </h4>
                                </div>
                            </div>
                            <!-- /product widget -->

                            <!-- product widget -->
                            <div class="product-widget">
                                <div class="product-img">
                                    <img src="{{ secure_asset('/electro') }}/img/product05.png" alt="" />
                                </div>
                                <div class="product-body">
                                    <p class="product-category">Category</p>
                                    <h3 class="product-name">
                                        <a href="{{ secure_asset('/electro') }}/#">product name goes here</a>
                                    </h3>
                                    <h4 class="product-price">
                                        $980.00 <del class="product-old-price">$990.00</del>
                                    </h4>
                                </div>
                            </div>
                            <!-- /product widget -->

                            <!-- product widget -->
                            <div class="product-widget">
                                <div class="product-img">
                                    <img src="{{ secure_asset('/electro') }}/img/product06.png" alt="" />
                                </div>
                                <div class="product-body">
                                    <p class="product-category">Category</p>
                                    <h3 class="product-name">
                                        <a href="{{ secure_asset('/electro') }}/#">product name goes here</a>
                                    </h3>
                                    <h4 class="product-price">
                                        $980.00 <del class="product-old-price">$990.00</del>
                                    </h4>
                                </div>
                            </div>
                            <!-- product widget -->
                        </div>

                        <div>
                            <!-- product widget -->
                            <div class="product-widget">
                                <div class="product-img">
                                    <img src="{{ secure_asset('/electro') }}/img/product07.png" alt="" />
                                </div>
                                <div class="product-body">
                                    <p class="product-category">Category</p>
                                    <h3 class="product-name">
                                        <a href="{{ secure_asset('/electro') }}/#">product name goes here</a>
                                    </h3>
                                    <h4 class="product-price">
                                        $980.00 <del class="product-old-price">$990.00</del>
                                    </h4>
                                </div>
                            </div>
                            <!-- /product widget -->

                            <!-- product widget -->
                            <div class="product-widget">
                                <div class="product-img">
                                    <img src="{{ secure_asset('/electro') }}/img/product08.png" alt="" />
                                </div>
                                <div class="product-body">
                                    <p class="product-category">Category</p>
                                    <h3 class="product-name">
                                        <a href="{{ secure_asset('/electro') }}/#">product name goes here</a>
                                    </h3>
                                    <h4 class="product-price">
                                        $980.00 <del class="product-old-price">$990.00</del>
                                    </h4>
                                </div>
                            </div>
                            <!-- /product widget -->

                            <!-- product widget -->
                            <div class="product-widget">
                                <div class="product-img">
                                    <img src="{{ secure_asset('/electro') }}/img/product09.png" alt="" />
                                </div>
                                <div class="product-body">
                                    <p class="product-category">Category</p>
                                    <h3 class="product-name">
                                        <a href="{{ secure_asset('/electro') }}/#">product name goes here</a>
                                    </h3>
                                    <h4 class="product-price">
                                        $980.00 <del class="product-old-price">$990.00</del>
                                    </h4>
                                </div>
                            </div>
                            <!-- product widget -->
                        </div>
                    </div>
                </div>

                <div class="clearfix visible-sm visible-xs"></div>

                <div class="col-md-4 col-xs-6">
                    <div class="section-title">
                        <h4 class="title">Top selling</h4>
                        <div class="section-nav">
                            <div id="slick-nav-5" class="products-slick-nav"></div>
                        </div>
                    </div>

                    <div class="products-widget-slick" data-nav="#slick-nav-5">
                        <div>
                            <!-- product widget -->
                            <div class="product-widget">
                                <div class="product-img">
                                    <img src="{{ secure_asset('/electro') }}/img/product01.png" alt="" />
                                </div>
                                <div class="product-body">
                                    <p class="product-category">Category</p>
                                    <h3 class="product-name">
                                        <a href="{{ secure_asset('/electro') }}/#">product name goes here</a>
                                    </h3>
                                    <h4 class="product-price">
                                        $980.00 <del class="product-old-price">$990.00</del>
                                    </h4>
                                </div>
                            </div>
                            <!-- /product widget -->

                            <!-- product widget -->
                            <div class="product-widget">
                                <div class="product-img">
                                    <img src="{{ secure_asset('/electro') }}/img/product02.png" alt="" />
                                </div>
                                <div class="product-body">
                                    <p class="product-category">Category</p>
                                    <h3 class="product-name">
                                        <a href="{{ secure_asset('/electro') }}/#">product name goes here</a>
                                    </h3>
                                    <h4 class="product-price">
                                        $980.00 <del class="product-old-price">$990.00</del>
                                    </h4>
                                </div>
                            </div>
                            <!-- /product widget -->

                            <!-- product widget -->
                            <div class="product-widget">
                                <div class="product-img">
                                    <img src="{{ secure_asset('/electro') }}/img/product03.png" alt="" />
                                </div>
                                <div class="product-body">
                                    <p class="product-category">Category</p>
                                    <h3 class="product-name">
                                        <a href="{{ secure_asset('/electro') }}/#">product name goes here</a>
                                    </h3>
                                    <h4 class="product-price">
                                        $980.00 <del class="product-old-price">$990.00</del>
                                    </h4>
                                </div>
                            </div>
                            <!-- product widget -->
                        </div>

                        <div>
                            <!-- product widget -->
                            <div class="product-widget">
                                <div class="product-img">
                                    <img src="{{ secure_asset('/electro') }}/img/product04.png" alt="" />
                                </div>
                                <div class="product-body">
                                    <p class="product-category">Category</p>
                                    <h3 class="product-name">
                                        <a href="{{ secure_asset('/electro') }}/#">product name goes here</a>
                                    </h3>
                                    <h4 class="product-price">
                                        $980.00 <del class="product-old-price">$990.00</del>
                                    </h4>
                                </div>
                            </div>
                            <!-- /product widget -->

                            <!-- product widget -->
                            <div class="product-widget">
                                <div class="product-img">
                                    <img src="{{ secure_asset('/electro') }}/img/product05.png" alt="" />
                                </div>
                                <div class="product-body">
                                    <p class="product-category">Category</p>
                                    <h3 class="product-name">
                                        <a href="{{ secure_asset('/electro') }}/#">product name goes here</a>
                                    </h3>
                                    <h4 class="product-price">
                                        $980.00 <del class="product-old-price">$990.00</del>
                                    </h4>
                                </div>
                            </div>
                            <!-- /product widget -->

                            <!-- product widget -->
                            <div class="product-widget">
                                <div class="product-img">
                                    <img src="{{ secure_asset('/electro') }}/img/product06.png" alt="" />
                                </div>
                                <div class="product-body">
                                    <p class="product-category">Category</p>
                                    <h3 class="product-name">
                                        <a href="{{ secure_asset('/electro') }}/#">product name goes here</a>
                                    </h3>
                                    <h4 class="product-price">
                                        $980.00 <del class="product-old-price">$990.00</del>
                                    </h4>
                                </div>
                            </div>
                            <!-- product widget -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- /row -->
        </div>
        <!-- /container -->
    </div>
    <!-- /SECTION -->

    <!-- NEWSLETTER -->
    <div id="newsletter" class="section">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <div class="col-md-12">
                    <div class="newsletter">
                        <p>Sign Up for the <strong>NEWSLETTER</strong></p>
                        <form>
                            <input class="input" type="email" placeholder="Enter Your Email" />
                            <button class="newsletter-btn">
                                <i class="fa fa-envelope"></i> Subscribe
                            </button>
                        </form>
                        <ul class="newsletter-follow">
                            <li>
                                <a href="{{ secure_asset('/electro') }}/#"><i class="fa fa-facebook"></i></a>
                            </li>
                            <li>
                                <a href="{{ secure_asset('/electro') }}/#"><i class="fa fa-twitter"></i></a>
                            </li>
                            <li>
                                <a href="{{ secure_asset('/electro') }}/#"><i class="fa fa-instagram"></i></a>
                            </li>
                            <li>
                                <a href="{{ secure_asset('/electro') }}/#"><i class="fa fa-pinterest"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- /row -->
        </div>
        <!-- /container -->
    </div>
    <!-- /NEWSLETTER -->

    <!-- FOOTER -->
    @include('electro.partial.footer')
    <!-- /FOOTER -->

    <!-- jQuery Plugins -->
    <script src="{{ secure_asset('/electro') }}/js/jquery.min.js"></script>
    <script src="{{ secure_asset('/electro') }}/js/bootstrap.min.js"></script>
    <script src="{{ secure_asset('/electro') }}/js/slick.min.js"></script>
    <script src="{{ secure_asset('/electro') }}/js/nouislider.min.js"></script>
    <script src="{{ secure_asset('/electro') }}/js/jquery.zoom.min.js"></script>
    <script src="{{ secure_asset('/electro') }}/js/main.js"></script>
</body>

</html>
