@extends('layouts.master')

@section('title')
    <h4>Halaman Edit Profile</h4>
@endsection

@section('content')
<div class="row">
    <div class="col">
        <div class="card">
            <div class="card-header">
                <a href="/profile" class="btn btn-primary">Kembali</a>
            </div>
            <div class="card-body">
                <form action="/profile/{{ $profile->id }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label for="full_name">Nama Lengkap</label>
                        <input type="text" class="form-control" name="full_name" value="{{ $profile->full_name }}" id="full_name"
                            placeholder="Masukkan Nama Lengkap">
                        @error('full_name')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="alamat">Alamat</label>
                        <textarea class="form-control" name="alamat" id="alamat" cols="150" rows="4">{{ $profile->alamat }}</textarea>
                        @error('alamat')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="bio">Bio</label>
                        <textarea class="form-control" name="bio" id="bio" cols="150" rows="4">{{ $profile->bio }}</textarea>
                        @error('bio')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="phone">Phone</label><br>
                        <input type="number" class="form-control" name="phone" value="{{ $profile->phone }}" id="phone"
                            placeholder="Masukkan Sku">
                        @error('phone')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <button type="submit" class="btn btn-warning">Update</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection