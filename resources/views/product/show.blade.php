@extends('layouts.master')

@section('title')

    Detail Product {{$product->id}}

@endsection

@section('content')

<div class="row">
    <div class="col-3">
        <img src="{{secure_asset('upload/product/'.$product->gambar)}}" width="100%" alt="">
    </div>
    <div class="col-9">
        <h4>Nama : {{$product->nama}}</h4>
        <h4>sku : {{$product->sku}}</h4>
        <h4>harga : {{$product->harga}}</h4>
        <h4>exp : {{$product->exp}}</h4>
        <h4>kategori_id : {{$product->kategori_id}}</h4>
        <a href="/product" class="btn btn-info btn-sm">Kembali</a>
    </div>
</div>

@endsection