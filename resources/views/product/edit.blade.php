@extends('layouts.master')

@section('title')
    <h4>Halaman Edit Product</h4>
@endsection

@section('content')
<div class="row">
    <div class="col">
        <div class="card">
            <div class="card-header">
                <h5 style="text-align: center">Preview Gambar</h5>
            </div>
            <div class="card-body">
                <img src="{{secure_asset('upload/product/'.$product->gambar)}}" class="rounded mx-auto d-block" style="" alt="" width="150px">
            </div>
        </div>
    </div>
</div>
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <a href="/product" class="btn btn-primary">Kembali</a>
                </div>
                <div class="card-body">
                <form action="/product/{{ $product->id }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label for="gambar">Gambar</label>
                        <input type="file" class="form-control" name="gambar" value="{{ $product->gambar }}" id="gambar"
                            placeholder="Masukkan Gambar">
                        @error('gambar')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="nama">Nama</label>
                        <input type="text" class="form-control" name="nama" value="{{ $product->nama }}" id="nama"
                            placeholder="Masukkan Nama">
                        @error('nama')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="sku">Sku</label>
                        <input type="number" class="form-control" name="sku" value="{{ $product->sku }}" id="sku"
                            placeholder="Masukkan Sku">
                        @error('sku')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="harga">Harga</label><br>
                        <input type="number" class="form-control" name="harga" value="{{ $product->harga }}" id="harga"
                            placeholder="Masukkan Sku">
                        @error('harga')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="exp">Exp</label><br>
                        <input type="date" class="form-control" name="exp" value="{{ $product->exp }}" id="exp"
                            placeholder="Masukkan Sku">
                        @error('exp')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="kategori">Kategori</label><br>
                        <select class="custom-select" name="kategori_id" id="kategori">
                            <option value="">--Silahkan Pilih Kategori--</option>
                            @foreach ($kategori as $item)
                                @if ($item->id_kategori === $product->kategori_id)
                                    <option value="{{ $item->id_kategori }}" selected>{{ $item->nama_kategori }}</option>
                                @else
                                    <option value="{{ $item->id_kategori }}">{{ $item->nama_kategori }}</option>
                                @endif
                            @endforeach
                        </select>
                        @error('kategori_id')
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                {{ $message }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @enderror
                    </div>
                    <button type="submit" class="btn btn-warning">Update</button>
                </form>
            </div>
        </div>
    </div>
    
</div>
@endsection
