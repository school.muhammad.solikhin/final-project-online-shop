@extends('layouts.master')

@push('css')
    <link rel="stylesheet" href="{{ secure_secure_asset('/adminLte') }}/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="{{ secure_secure_asset('/adminLte') }}/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
    <link rel="stylesheet" href="{{ secure_secure_asset('/adminLte') }}/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
@endpush

@section('title')
    <h5>Halaman Data Kategori</h5>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">

            <div class="card">
                <div class="card-header">
                    @auth
                    <a href="/kategori/create" class="btn btn-primary">Tambah Data</a>
                    @endauth
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <table id="kategori" class="table table-bordered table-striped" style="text-align: center">
                        <thead>
                            <tr>
                                <th>NO</th>
                                <th>Nama</th>
                                <th>Created at</th>
                                <th>Updated at</th>
                                <th>Update</th>
                                <th>Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($kategori as $key=>$value)
                                <tr>
                                    <td>{{ $key + 1 }}</th>
                                    <td>{{ $value->nama_kategori }}</td>
                                    <td>{{ $value->created_at }}</td>
                                    <td>{{ $value->updated_at }}</td>
                                    <td><a href="/kategori/{{ $value->id_kategori }}/edit" class="btn btn-primary"><i class="fas fa-edit"></i></a></td>
                                    <td>
                                        <form action="/kategori/{{ $value->id_kategori }}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-danger"><i class="fas fa-trash"></i></button>
                                        </form>
                                    </td>
                                </tr>
                            @empty
                                <tr style="text-align: center">
                                    <td colspan="6">No data Available</td>
                                </tr>
                            @endforelse
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>NO</th>
                                <th>Nama</th>
                                <th>Created at</th>
                                <th>Update at</th>
                                <th>Update</th>
                                <th>Delete</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <!-- /.col -->
    </div>
@endsection

@push('script')
    <script src="{{ secure_secure_asset('/adminLte') }}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{{ secure_secure_asset('/adminLte') }}/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="{{ secure_secure_asset('/adminLte') }}/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
    <script src="{{ secure_secure_asset('/adminLte') }}/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
    <script src="{{ secure_secure_asset('/adminLte') }}/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
    <script src="{{ secure_secure_asset('/adminLte') }}/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
    <script src="{{ secure_secure_asset('/adminLte') }}/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
    <script src="{{ secure_secure_asset('/adminLte') }}/plugins/datatables-buttons/js/buttons.print.min.js"></script>
    <script src="{{ secure_secure_asset('/adminLte') }}/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
    <script>
        $(function() {
            $("#kategori").DataTable({
                "responsive": true,
                "lengthChange": false,
                "autoWidth": false,
                "buttons": ["copy", "print", "colvis"]
            }).buttons().container().appendTo('#kategori_wrapper .col-md-6:eq(0)');
        });
    </script>
@endpush
