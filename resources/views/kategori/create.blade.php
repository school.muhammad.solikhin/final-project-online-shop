@extends('layouts.master')

@push('css')

@endpush

@section('title')
    <h5>Halaman Tambah Data Kategori</h5>
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
               <a href="/kategori" class="btn btn-primary">Kembali</a>
            </div>
            <div class="card-body">
                <form action="/kategori" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="judul">Nama Kategori</label>
                        <input type="text" class="form-control" name="nama" id="nama" placeholder="Masukkan Nama">
                        @error('nama')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <button type="submit" class="btn btn-primary">Tambah</button>
                    <button type="reset" class="btn btn-warning">Reset</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@push('script')

@endpush
