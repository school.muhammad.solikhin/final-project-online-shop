<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Laporan Data Kategori</title>
    <style>
        table {
            width: 100%;
            border-collapse: collapse;
            margin: 50px auto;
        }

        /* Zebra striping */
        tr:nth-of-type(odd) {
            background: #eee;
        }

        th {
            background: #3498db;
            color: white;
            font-weight: bold;
        }

        td,
        th {
            padding: 10px;
            border: 1px solid #ccc;
            text-align: left;
            font-size: 18px;
        }

     
    </style>
</head>

<body>
    <h1 style="text-align: center">Laporan Data Kategori</h1>
    <table>
        <thead>
            <tr>
                <th>No</th>
                <th>Nama Kategori</th>
                <th>Created at</th>
                <th>Update at</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($kategori as $key=>$value)
                <tr>
                    <td>{{ $key + 1 }}</th>
                    <td>{{ $value->nama_kategori }}</td>
                    <td>{{ $value->created_at }}</td>
                    <td>{{ $value->updated_at }}</td>
                </tr>
            @empty
                <tr style="text-align: center">
                    <td colspan="4">No data Available</td>
                </tr>
            @endforelse
        </tbody>
    </table>
</body>

</html>
