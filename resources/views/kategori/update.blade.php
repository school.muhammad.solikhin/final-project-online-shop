@extends('layouts.master')

@push('css')

@endpush

@section('title')
    <h5>Halaman Edit Data Kategori</h5>
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
               <a href="/kategori" class="btn btn-primary">Kembali</a>
            </div>
            <div class="card-body">
                <form action="/kategori/{{$kategori->id}}" method="post">
                    @csrf
                    @method('put')
                    <div class="form-group">
                        <label for="judul">Nama Kategori</label>
                        <input type="text" value="{{$kategori->nama}}" class="form-control" name="nama" id="nama" placeholder="Masukkan Nama">
                        @error('nama')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <button type="submit" class="btn btn-primary">Edit</button>
                    <button type="reset" class="btn btn-warning">Reset</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@push('script')

@endpush
