<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
        <div class="ml-3">
            <i class="fab fa-shopify fa-lg"></i>
            <span class="brand-text font-weight-light">Online Shop</span>
        </div>

    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{ secure_asset('/adminLte') }}/dist/img/user2-160x160.jpg" class="img-circle elevation-2"
                    alt="User Image">
            </div>
            <div class="info">
                @auth
                    <a href="#" class="d-block">{{ Auth::user()->name }}</a>
                @endauth
                @guest
                    <a href="#" class="d-block">Belum Login</a>
                @endguest
            </div>
        </div>

        <!-- SidebarSearch Form -->
        <div class="form-inline">
            <div class="input-group" data-widget="sidebar-search">
                <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
                <div class="input-group-append">
                    <button class="btn btn-sidebar">
                        <i class="fas fa-search fa-fw"></i>
                    </button>
                </div>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
       with font-awesome or any other icon font library -->
                @guest
                    <li class="nav-item">
                        <a href="/" class="nav-link">
                            <i class="nav-icon fas fa-home"></i>
                            <p>
                                Dashboard
                            </p>
                        </a>
                    </li>
                @endguest
                @auth
                    @switch(Auth::user()->level)
                        @case('admin')
                            <li class="nav-item">
                                <a href="/profile" class="nav-link">
                                    <i class="nav-icon fas fa-user-alt"></i>
                                    <p>
                                        Profile
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="/seller" class="nav-link">
                                    <i class="nav-icon fas fa-users"></i>
                                    <p>
                                        Data Seller
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="/product" class="nav-link">
                                    <i class="nav-icon fas fa-bars"></i>
                                    <p>
                                        Product
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="/order" class="nav-link">
                                    <i class="nav-icon fas fa-sort-amount-down-alt"></i>
                                    <p>
                                        Order
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="/kategori" class="nav-link">
                                    <i class="nav-icon fas fa-border-all"></i>
                                    <p>
                                        Kategori
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="nav-link">
                                    <i class="nav-icon fas fa-file-alt"></i>
                                    <p>
                                        Laporan
                                        <i class="right fas fa-angle-left"></i>
                                    </p>
                                </a>
                                <ul class="nav nav-treeview">
                                    <li class="nav-item">
                                        <a href="/kategori/laporan" class="nav-link">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Data Kategori</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="/product/laporan" class="nav-link">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Data Product</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="/seller/laporan" class="nav-link">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Data Seller</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="/product/laporan" class="nav-link">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Data Product</p>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        @break

                        @case('penjual')
                            <li class="nav-item">
                                <a href="/profile/{{ Auth::user()->id }}" class="nav-link">
                                    <i class="nav-icon fas fa-user-alt"></i>
                                    <p>
                                        Profile
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="/product" class="nav-link">
                                    <i class="nav-icon fas fa-bars"></i>
                                    <p>
                                        Product
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="/kategori" class="nav-link">
                                    <i class="nav-icon fas fa-border-all"></i>
                                    <p>
                                        Kategori
                                    </p>
                                </a>
                            </li>
                        @break

                        @case('pembeli')
                            <li class="nav-item">
                                <a href="/profile/{{ Auth::user()->id }}" class="nav-link">
                                    <i class="nav-icon fas fa-user-alt"></i>
                                    <p>
                                        Profile
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="/" class="nav-link">
                                    <i class="nav-icon fas fa-cart-plus"></i>
                                    <p>
                                        Start Shopping !
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="/cart" class="nav-link">
                                    <i class="nav-icon fas fa-shopping-cart"></i>
                                    <p>
                                        Cart
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="/cart" class="nav-link">
                                    <i class="nav-icon fas fa-credit-card"></i>
                                    <p>
                                        Checkout
                                    </p>
                                </a>
                            </li>
                        @break

                        @default

                    @endswitch

                @endauth
                @auth
                    <li class="nav-item">
                        <!-- <a id="navbarDropdown" class="nav-link" href="#" role="button"></a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown"> -->
                        <a class="nav-link" href="{{ route('logout') }}"
                            onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            <i class="fas fa-sign-out-alt nav-icon"></i>Logout
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                        <!-- </div> -->
                    </li>
                @endauth
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
