@extends('orders.master')

@section('content')
@foreach ($orders as $key => $order)    
<div class="container pt-5">
  <div class="card">
    <div class="card-header">
      <h3 class="card-title">No Pesanan : 00{{$key+1}} </h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body p-0">
      <table class="table table-sm">
        <thead>
          <tr>
            <th style="text-align: center" colspan="2">Konfirmasi Pemesanan {{$order->user_id}}</th>
          </tr>
        </thead>
        <thead>
            <tr>
              <th></th>
              <th style="text-align: right">
                <a href="/order/{{$order->id}}/edit" class="btn btn-light btn-sm">Ubah</a>
                <a href="/order/{{$order->id}}" class="btn btn-info btn-sm">Payment</a>
              </th>
            </tr>
          </thead>
        <tbody>
          <tr>
            <td>Tanggal Order</td>
            <td>: {{$order -> tgl_order}} </td>
          </tr>
          <tr>
            <td>Nama Produk</td>
            <td>: Abrelco</td>
          </tr>
          <tr>
            <td>Jumlah Item</td>
            <td>: {{ $order-> jmlh_barang }} </td>
          </tr>
          <tr>
            <td>Harga Barang</td>
            <td>: 150.000</td>
          </tr>
          <tr>
            <td colspan="2" style="text-align: left">
                <form action="order/{{$order->id}}" method="POST">
                    @csrf
                    @method('DELETE')
                    <input type="submit" value="Batalkan" class="btn btn-danger btn-sm">
                </form>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <!-- /.card-body -->
  </div>
</div>  
@endforeach
@endsection