@extends('orders.master')

@section('content')
<div class="container pt-5">
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">No Pesanan : 00</h3>
      </div>
      <!-- /.card-header -->
      <div class="card-body p-0">
        <table class="table table-sm">
          <thead>
            <tr>
              <th style="text-align: center" colspan="2">Konfirmasi Pemesanan</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Tanggal Order</td>
              <td>: {{$orders->tgl_order}} </td>
            </tr>
            <tr>
              <td>Nama Produk</td>
              <td>: Abrelco</td>
            </tr>
            <tr>
              <td>Jumlah Item</td>
              <td>: {{$orders->jmlh_barang}}</td>
            </tr>
            <tr>
              <td>Harga Barang</td>
              <td>: 150.000</td>
            </tr>
            <tr>
              <td>Pembayaran</td>
              <td>: 
                <button type="button" class="btn btn-success">Sudah dibayar</button>
              </td>
            </tr>
            <tr>
              <td>Status</td>
              <td>: Sedang dalam proses pengiriman</td>
            </tr>
          </tbody>
        </table>
      </div>
      <!-- /.card-body -->
    </div>
</div>  
    
@endsection