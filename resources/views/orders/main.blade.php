<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <title>Ecommerce</title>

    <!-- Google font -->
    <link href="{{ secure_asset('/electro') }}/https://fonts.googleapis.com/css?family=Montserrat:400,500,700"
        rel="stylesheet" />

    <!-- Bootstrap -->
    <link type="text/css" rel="stylesheet" href="{{ secure_asset('/electro') }}/css/bootstrap.min.css" />

    <!-- Slick -->
    <link type="text/css" rel="stylesheet" href="{{ secure_asset('/electro') }}/css/slick.css" />
    <link type="text/css" rel="stylesheet" href="{{ secure_asset('/electro') }}/css/slick-theme.css" />

    <!-- nouislider -->
    <link type="text/css" rel="stylesheet" href="{{ secure_asset('/electro') }}/css/nouislider.min.css" />

    <!-- Font Awesome Icon -->
    <link rel="stylesheet" href="{{ secure_asset('/electro') }}/css/font-awesome.min.css" />

    <!-- Custom stlylesheet -->
    <link type="text/css" rel="stylesheet" href="{{ secure_asset('/electro') }}/css/style.css" />

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="{{ secure_asset('/electro') }}/https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="{{ secure_asset('/electro') }}/https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <!-- HEADER -->
    @include('electro.partial.header')
    <!-- /HEADER -->

    <!-- NAVIGATION -->
    @include('electro.partial.navbar')
    <!-- /NAVIGATION -->
    <!-- SECTION -->

    <!-- /SECTION -->
    <!-- div class="section">
        <div class="container">
            <div class="row fullscreen align-items-center justify-content-center" style="height: 634px">
                <div class="col-lg-6 col-md-12 d-flex align-self-end img-right no-padding">
                    <img id="img" class="img-fluid" src="{{secure_asset('img/test.jpg')}}" alt="">
                </div>
                <div class="banner-content col-lg-6 col-md-12">
                    <h1 class="title-top">
                        <span>Flat</span>
                        75% Off
                    </h1>
                    <h1 class="text-uppercase">
                        It's Happening
                        <br>
                        This Season
                    </h1>
                    <button class="primary-btn text-uppercase">
                        <a style="color: white" href="">Purchase Now</a>
                    </button>
                </div>
            </div>
        </div>
    </div-->
    <!-- SECTION -->
    
    <!-- /SECTION -->

    <!-- SECTION -->
    <div class="section">
    @yield('content')
    </div>
    <!-- /SECTION -->

    
    <!-- SECTION -->
    
 
    <!-- FOOTER -->
 
    <!-- /FOOTER -->

    <!-- jQuery Plugins -->
    <script src="{{ secure_asset('/electro') }}/js/jquery.min.js"></script>
    <script src="{{ secure_asset('/electro') }}/js/bootstrap.min.js"></script>
    <script src="{{ secure_asset('/electro') }}/js/slick.min.js"></script>
    <script src="{{ secure_asset('/electro') }}/js/nouislider.min.js"></script>
    <script src="{{ secure_asset('/electro') }}/js/jquery.zoom.min.js"></script>
    <script src="{{ secure_asset('/electro') }}/js/main.js"></script>
</body>

</html>
