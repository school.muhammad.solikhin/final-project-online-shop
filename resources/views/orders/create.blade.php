@extends('orders.master')

@section('content')
<div class="container pt-5">
  <div class="row">
      <div class="col-6">
        <div class="col-md-4 col-xs-6">
                <div> 
                    <img src="http://localhost:8000/upload/product/1628393346 - third.jpg" alt="Product">
                </div>
        </div>
      </div>
      <div class="col-6">
        <div class="card">
            <h5 class="card-header"> Abrelco </h5>
            <div class="card-body">
              <h5 class="card-title">Special title treatment</h5>
              <p class="card-text">With supporting .</p>
              <br>
              <form action="/order" method="POST">
                  @csrf
                <div class="form-row align-items-center">
                  <div class="col-2">
                    <label for="jmlh_barang">Jumlah</label>
                    <input type="text" class="form-control mb-2" id="jmlh_barang" placeholder="   1" name="jmlh_barang">
                    @error('jmlh_barang')
    		            <div class="alert alert-danger">{{ $message }}</div>
		            @enderror
                  </div>
                  <div class="col-4">
                    <label for="tgl_order">Tanggal Pemesanan</label>
                    <div class="input-group date mb-2" id="reservationdate" data-target-input="nearest">
                        <input type="date" class="form-control" name="tgl_order">
                        @error('tgl_order')
    		                <div class="alert alert-danger">{{ $message }}</div>
		                @enderror
                    </div>
                  </div>
                  <div class="col-3">
                    <label for="user">Pembeli</label>
                    <select class="custom-select mb-2" id="user" name="user">
                        @foreach ($users as $user)
                            <option value=" {{$user->id}} ">{{$user->name}}</option>
                        @endforeach
                    </select>
                    @error('user')
    		            <div class="alert alert-danger">{{ $message }}</div>
		            @enderror
                  </div>
                  <div class="col-3">
                    <button type="submit" class="btn btn-dark mb-n3">Checkout</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
      </div>
  </div>
</div>
@endsection