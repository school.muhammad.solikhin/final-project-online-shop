@extends('layouts.master')

@section('title')

<h4>Add Seller Account</h4>

@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
               <a href="/seller" class="btn btn-primary">Kembali</a>
            </div>
            <div class="card-body">
                <form action="/seller" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="judul">Nama Lengkap</label>
                        <input type="text" class="form-control" name="nama" id="nama" placeholder="Masukkan Nama Lengkap">
                        @error('nama')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="judul">Email</label>
                        <input type="email" class="form-control" name="email" id="email" placeholder="Masukkan Email">
                        @error('email')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="judul">Password</label>
                        <input type="password" class="form-control" name="password" id="email" placeholder="Masukkan password">
                        @error('email')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="judul">Alamat Lengkap</label>
                        <textarea name="alamat" id="" cols="30" rows="4" class="form-control"></textarea>
                        @error('alamat')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="judul">Bio</label>
                        <textarea name="bio" id="" cols="30" rows="4" class="form-control"></textarea>
                        @error('bio')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="judul">No HandPhone</label>
                        <input type="number" class="form-control" name="phone" id="nama" placeholder="Masukkan No Hand Phone">
                        @error('phone')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <button type="submit" class="btn btn-primary">Tambah</button>
                    <button type="reset" class="btn btn-warning">Reset</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection