<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Laporan Data Seller</title>
    <style>
        table {
            width: 100%;
            border-collapse: collapse;
            margin: 50px auto;
        }

        /* Zebra striping */
        tr:nth-of-type(odd) {
            background: #eee;
        }

        th {
            background: #3498db;
            color: white;
            font-weight: bold;
        }

        td,
        th {
            padding: 10px;
            border: 1px solid #ccc;
            text-align: left;
            font-size: 18px;
        }

     
    </style>
</head>

<body>
    <h1 style="text-align: center">Laporan Data Seller</h1>
    <table>
        <thead>
            <tr>
                <th>No</th>
                <th>Nama Seller</th>
                <th>Alamat</th>
                <th>Phone</th>
                <th>Email</th>
                <th>Created at</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($join as $key=>$value)
                <tr>
                    <td>{{ $key + 1 }}</th>
                    <td>{{ $value->full_name }}</td>
                    <td>{{ $value->alamat }}</td>
                    <td>{{ $value->phone }}</td>
                    <td>{{ $value->email }}</td>
                    <td>{{ $value->created_at }}</td>
                </tr>
            @empty
                <tr style="text-align: center">
                    <td colspan="6">No data Available</td>
                </tr>
            @endforelse
        </tbody>
    </table>
</body>

</html>
