<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

## Anggota Kelompok 9 : 

1. Abdullah Zakaria (@iamdzk)
1. Muhammad Solikhin (@Khinnnnn)
1. Waliyyudin (@walitsaz1286)

## Tema yang di angkat :

Online Shop

## Link Demo Website

https://drive.google.com/file/d/1ykZVqJin6beZRiBO4-JaXr_4PEugxP-l/view?usp=sharing

## Link Hosting

https://dry-eyrie-63385.herokuapp.com/

## ERD Online Shop

![gambar erd](/public/img/erd.jpg)