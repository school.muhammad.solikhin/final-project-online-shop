<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Detail_Product extends Model
{
    protected $table = "detail_product";
    protected $fillable = ['stock','deskripsi','seller_id','product_id'];
    // protected $guarded = [];

    public function seller() {
        $this->belongsTo('App\Seller');
    }

    public function product() {
        $this->belongsTo('App\Product');
    }

    public function order() {
        $this->belongsTo('App\Order');
    }
}
