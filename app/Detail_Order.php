<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Detail_order extends Model
{
    public function purchase() {
        return $this->belongsTo('App\Order');
    }

    public function item() {
        return $this->belongsTo('App\Product', 'productid');
    }
}
