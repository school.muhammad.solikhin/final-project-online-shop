<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Product;

class Category extends Model
{
    protected $table = "kategori";
    protected $fillable = ["nama_kategori"];

    public function product() {
        $this->hasMany('App\Product');
    }
}
