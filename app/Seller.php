<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Seller extends Model
{
    protected $table = "seller";
    //protected $fillable = ['photo','full_name','alamat','bio','phone','user_id'];
    protected $fillable = ['nama_seller','password','user_id'];
    // protected $guarded = [];

    public function user() {
        $this->belongsTo('App\User');
    }

    public function detailProduk() {
        $this->hasMany('App\Detail_Product');
    }
    
}
