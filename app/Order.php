<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Order extends Model
{
    protected $table = "orders";

    public function users() {
       return $this->hasOne('App\User', 'user_id');
    }

    public function items() {
        return $this->belongsToMany('App\Product', 'order_product', 'order_id', 'productid');
    }

    public function detail() {
        return $this->hasMany('App\Detail_order');
     }

    public function detailOrder() {
        $this->hasMany('App\Detail_Order');
    }
}
