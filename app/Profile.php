<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table = "profiles";
    protected $fillable = ['full_name','alamat','bio','phone','user_id'];
    // protected $guarded = [];

    public function user() {
        $this->belongsTo('App\User');
    }
}
