<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Category;

class Product extends Model
{
    protected $table = "product";
    protected $fillable = ["gambar","nama","sku","harga","exp","kategori_id"];
    // protected $guarded = [];

    public function kategori() {
        $this->belongsTo('App\Category');
    }
    
    public function detailProduct() {
        $this->hasMany('App\Detail_Product');
    }
    
    public function detailOrder() {
        $this->hasMany('App\Detail_Order');
    }
}
