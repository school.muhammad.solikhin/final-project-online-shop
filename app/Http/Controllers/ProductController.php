<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Category;
use Auth;
use DB;
use File;

class ProductController extends Controller
{
    public function __construct(){
        $this->middleware('auth')->except('index','show');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $join = DB::table('product')
            ->join('kategori', 'product.kategori_id', '=', 'kategori.id_kategori')
            ->select('product.*', 'kategori.*')
            ->get();
        $kategori = Category::all(); //buat yang kategori //ini buat dropdown nya
        return view('product.index', compact('kategori','join'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kategori = Category::all();
        return view('product.create', compact('kategori'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'gambar' => 'required|mimes:jpeg,jpg,png|max:2200',
            'nama' => 'required',
            'sku' => 'required',
            'harga' => 'required',
            'exp' => 'required',
            'kategori_id' => 'required'
        ]);

        $gambar = $request->gambar;
        $new_gambar = time() . ' - ' . $gambar->getClientOriginalName();
            
        // $query = DB::table('product')->insert([
        //         "gambar" => $new_gambar,
        //         "nama" => $request['nama'],
        //         "sku" => $request['sku'],
        //         "harga" => $request['harga'],
        //         "exp" => $request['exp'],
        //         "kategori_id" => $request['kategori']
        //     ]);

        $product = Product::create([
            "gambar" => $new_gambar,
            "nama" => $request->nama,
            "sku" => $request->sku,
            "harga" => $request->harga,
            "exp" => $request->exp,
            "kategori_id" => $request->kategori_id
        ]);

        $gambar->move('upload/product/', $new_gambar);
        return redirect('/product');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::findorfail($id);
        return view('product.show', compact('product'));        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::findorfail($id);
        $kategori = Category::all();
        return view('product.edit', compact('product', 'kategori'));        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        $request->validate([
            'gambar' => 'mimes:jpeg,jpg,png|max:2200',
            'nama' => 'required',
            'sku' => 'required',
            'harga' => 'required',
            'exp' => 'required',
            'kategori_id' => 'required'
        ]);

        $product = Product::findorfail($id);

        if ($request->has('gambar')){
            $path = "upload/product/";
            File::delete($path . $product->gambar);
            $gambar = $request->gambar;
            $new_gambar = time() . ' - ' . $gambar->getClientOriginalName();
            $gambar->move('upload/product/', $new_gambar);
            $post_data = [
                "gambar" => $new_gambar,
                "nama" => $request->nama,
                "sku" => $request->sku,
                "harga" => $request->harga,
                "exp" => $request->exp,
                "kategori_id" => $request->kategori_id
            ];
        }else{
            $post_data = [
                "nama" => $request->nama,
                "sku" => $request->sku,
                "harga" => $request->harga,
                "exp" => $request->exp,
                "kategori_id" => $request->kategori_id
            ];
        }

        $product->update($post_data);
        return redirect('/product');    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::findorfail($id);
        $product->delete();

        // $deletedRows = Product::where('id', $id)->delete(); //opsi 1
        // Product::destroy($id); //opsi 2
        $path = "upload/product/";
        File::delete($path . $product->gambar);

        return redirect()->route('product.index');        
    }
}
