<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use App\Profile;
use App\Seller;
use DB;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'nama' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'alamat' => ['required', 'string', 'max:255'],
            'phone' => ['required', 'string', 'max:255'],
            'level' => ['required'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        if($data['level'] === 'pembeli'){
            $user = User::create([
                'name' => $data['nama'],
                'email' => $data['email'],
                'password' => Hash::make($data['password']),
                'level' => $data['level'],
            ]);

            $query = User::select('id')->take(1)->orderBy('id', 'DESC')->get();

            Profile::create([
                'photo' => '',
                'full_name' => $data['nama'],
                'alamat' => $data['alamat'],
                'bio' => '',
                'phone' => $data['phone'],
                'user_id' => $query[0]->id,
            ]);
            return $user;

        } else if($data['level'] === 'penjual'){
            $user = User::create([
                'name' => $data['nama'],
                'email' => $data['email'],
                'password' => Hash::make($data['password']),
                'level' => $data['level'],
            ]);
    
            $query = User::select('id')->take(1)->orderBy('id', 'DESC')->get();
            
            Profile::create([
                'photo' => '',
                'full_name' => $data['nama'],
                'alamat' => $data['alamat'],
                'bio' => '',
                'phone' => $data['phone'],
                'user_id' => $query[0]->id,
            ]);
    
            Seller::create([
                'nama_seller' => $data['nama'],
                'password' => Hash::make($data['password']),
                'user_id' => $query[0]->id,
            ]);
            
            return $user;
        }
        
    }
}
