<?php

namespace App\Http\Controllers;

use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use App\Seller;
use App\User;
use App\Profile;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use DB;
use PDF;
use App\Exports\SellerExport;
use Maatwebsite\Excel\Facades\Excel;

class SellerController extends Controller
{
    public function __construct() {
        $this->middleware('auth')->except('');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::all();
        $seller = Seller::all();
        $join = DB::table('profiles')
        ->join('users', 'profiles.user_id', '=', 'users.id')
        ->select('profiles.*', 'users.email')
        ->get();

        return view('seller.index', compact('join'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'nama_seller' => ['required', 'string', 'max:255'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'user_id' => ['required']
        ]);
    }

    public function create() {
    
        return view('seller.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $this->validate($request,[
            'nama' => 'required|',
            'email' => 'required',
            'password' => 'required',
            'alamat' => 'required',
            'bio' => 'required',
            'phone' => 'required'
        ]);
        
        User::create([
            'name' => $request->nama,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'level' => 'penjual'
        ]);

        $query = User::select('id')->take(1)->orderBy('id', 'DESC')->get();
        
        Profile::create([
            'photo'=> 'test',
            'full_name' => $request->nama,
            'alamat' => $request->alamat,
            'bio' => $request->bio,
            'phone' => $request->phone,
            'user_id' => $query[0]->id,
        ]);

        Seller::create([
            'nama_seller' => $request->nama,
            'password' => Hash::make($request->password),
            'user_id' => $query[0]->id,
        ]);
        

        return redirect('/seller');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $seller = Seller::find($id);
        return view('seller.edit', compact('seller'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate([
            'nama_seller' => 'required',
            'password' => 'required'
        ]);

        $post = Seller::find($id);
        $post->nama_seller = $request->nama_seller;
        $post->update();
        return redirect('/seller');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Seller::find($id);
        $post->delete();
        return redirect('/seller');        
    }

    public function laporan()
    {
        $join = DB::table('profiles')
        ->join('users', 'profiles.user_id', '=', 'users.id')
        ->join('seller', 'seller.user_id', '=', 'users.id')
        ->select('profiles.*', 'users.email')
        ->where('level','penjual')
        ->get();

        return view('seller.laporan', compact('join'));
    }

    public function pdf()
    {
        $join = DB::table('profiles')
        ->join('users', 'profiles.user_id', '=', 'users.id')
        ->join('seller', 'seller.user_id', '=', 'users.id')
        ->select('profiles.*', 'users.email')
        ->where('level','penjual')
        ->get();

        $pdf = PDF::loadView('seller.pdf', compact('join'));
        return $pdf->download('laporan_seller.pdf');
    }

    public function export() 
    {
        return Excel::download(new SellerExport, 'Seller.xlsx');
    }
}
