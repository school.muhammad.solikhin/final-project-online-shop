<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Profile;
use App\User;
use DB;
use File;
use Auth;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $join = DB::table('profiles') 
            ->join('users', 'profiles.user_id', '=', 'users.id') 
            ->select('profiles.*', 'users.email', 'users.level') 
            ->get();
        $user = User::all();
        $profile = Profile::where('user_id', Auth::user()->id)->first();
        return view('profile.index', compact('join','profile','user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'full_name' => 'required',
            'alamat' => 'required',
            'bio' => 'required',
            'phone' => 'required'
        ]);
        
        Profile::create([
            'full_name' => $request->full_name,
            'alamat' => $request->alamat,
            'bio' => $request->bio,
            'phone' => $request->phone,
            'user_id' => Auth::user()->id
        ]);

        return redirect('/profile');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $profile = Profile::findorfail($id);
        $user = User::all();
        return view('profile.edit', compact('profile', 'user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate([
            'full_name' => 'required',
            'alamat' => 'required',
            'bio' => 'required',
            'phone' => 'required'
        ]);

        $post = Profile::find($id);
        $post->full_name = $request->full_name;
        $post->alamat = $request->alamat;
        $post->bio = $request->bio;
        $post->phone = $request->phone;
        $post->update();

        return redirect('/profile');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $profile = Profile::findorfail($id);
        $profile->delete();

        return redirect()->route('profile.index');        
    }
}
