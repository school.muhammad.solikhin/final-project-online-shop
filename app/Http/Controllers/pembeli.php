<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use DB;

class pembeli extends Controller
{
    /*public function __construct() {
        $this->middleware('auth')->except('');
    }*/
    public function index()
    {
        $Product = DB::table('product')->take(3)->get();
        $join = DB::table('product')
        ->join('kategori', 'product.kategori_id', '=', 'kategori.id_kategori')
        ->select('product.*', 'kategori.*')
        ->get();
        return view('electro.master', compact('Product','join'));
    }
    public function cart()
    {
        return view('electro.cart');
    }

    public function checkout()
    {
        return view('electro.checkout');
    }
}
