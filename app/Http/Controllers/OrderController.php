<?php

namespace App\Http\Controllers;

use App\User;
use App\Product;
use App\Detail_order;
use App\Order;
use Illuminate\Http\Request;
use DB;

class OrderController extends Controller
{
    public function create() {
        $item = Product::all();
        
        // dd($item);
        $users = User::all();
        return view('orders.create')->with('users', $users);
    }

    public function store(Request $request) {
        $request->validate([
		    'jmlh_barang' => 'required',
		    'tgl_order' => 'required',
		    'ringkasan' => 'required'
		]);

        // $query = DB::table('orders')->insert([
        //     'jmlh_barang'=>$request['jmlh_barang'],
    	// 	'tgl_order'=>$request['tgl_order'],
    	// 	'user_id' => $request['user']
        // ]);

        $orders = new Order;
        $orders->jmlh_barang = $request["jmlh_barang"];
        $orders->tgl_order = $request["tgl_order"];
        $orders->user_id = $request["user"];
        $orders->save();


        return redirect('/order/create');
    }

    public function index() {
        // $user = users();
        // $orders = DB::table('orders')->get();
        $orders = Order::all();
        return view('orders.index', compact('orders')); 
    }

    public function show($id) {
        // $order =DB::table('orders')->where('id',$id)->first();
        // $jOrder = DB::table('orders')
        //         ->join('detail_orders','detail_orders.order_id','=','orders.id')
        //         ->select('orders.*','detail_orders.status','detail_orders.pembayaran')
        //         ->get();

        // // dd($jOrder);


        $orders = Order::find($id);
        return view('orders.show', compact('orders'));
    }

    public function edit($id) {
        $order =DB::table('orders')->where('id',$id)->first();
        return view('orders.edit', compact('order'));
    }

    public function update($id, Request $request) {
        $query = DB::table('orders')
                    ->where('id',$id)
                    ->update([
                        'tgl_order' =>$request['tgl_order'],
                        'jmlh_barang' =>$request['jmlh_barang'],
                        'user_id' =>$request['user']
                    ]);
        return redirect('/order');
    }

    public function destroy($id) {
        // $query = DB::table('users')->where('id', $id)->delete();
        Order::destroy($id);
        
        return redirect('/order');
    }
}
