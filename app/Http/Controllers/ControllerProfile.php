<?php

namespace App\Http\Controllers;
use App\Profile;
use App\User;
use DB;
use File;
use Illuminate\Http\Request;

class ControllerProfile extends Controller
{
    public function index()
    {
        $join = DB::table('profiles') 
            ->join('users', 'profiles.user_id', '=', 'users.id') 
            ->select('profiles.*', 'users.email', 'users.level') 
            ->get();
        $user = User::all();
        return view('profile.index', compact('join','user'));
    }

    public function show($id)
    {
        $join = DB::table('profiles') 
            ->join('users', 'profiles.user_id', '=', 'users.id') 
            ->select('profiles.*', 'users.email', 'users.level') 
            ->get();
        $user = User::all();
        return view('profile.index', compact('join','user'));
    }

    public function edit($id)
    {
        $profile = Profile::findorfail($id);
        $user = User::all();
        return view('profile.edit', compact('profile', 'user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'photo' => 'mimes:jpeg,jpg,png|max:2200',
            'full_name' => 'required',
            'alamat' => 'required',
            'bio' => 'required',
            'phone' => 'required',
            'user_id' => 'required'
        ]);

        $profile = Profile::findorfail($id);

        if ($request->has('photo')){
            $path = "upload/product/";
            File::delete($path . $profile->photo);
            $photo = $request->photo;
            $new_photo = time() . ' - ' . $photo->getClientOriginalName();
            $photo->move('upload/profile/', $new_photo);
            $post_data = [
                "photo" => $new_photo,
                "full_name" => $request->full_name,
                "alamat" => $request->alamat,
                "bio" => $request->bio,
                "phone" => $request->phone,
                "user_id" => $request->user_id
            ];
        }else{
            $post_data = [
                "full_name" => $request->full_name,
                "alamat" => $request->alamat,
                "bio" => $request->bio,
                "phone" => $request->phone,
                "user_id" => $request->user_id
            ];
        }

        $profile->update($post_data);
        return redirect('/profile');    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $profile = Profile::findorfail($id);
        $profile->delete();

        $path = "upload/profile/";
        File::delete($path . $profile->photo);

        return redirect()->route('profile.index');        
    }
}
