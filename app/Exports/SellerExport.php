<?php

namespace App\Exports;

use App\Seller;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use DB;

class SellerExport implements FromCollection, WithHeadings, ShouldAutoSize, WithEvents
{
    /**
    * @return \Illuminate\Support\Collection
    */
    
    public function collection()
    {
        $join = DB::table('profiles')
        ->join('users', 'profiles.user_id', '=', 'users.id')
        ->join('seller', 'seller.user_id', '=', 'users.id')
        ->select('profiles.*', 'users.email')
        ->where('level','penjual')
        ->get();

        return $join;
    }

    public function headings(): array
    {
        return [
            'NO',
            'Nama Seller',
            'Alamat',
            'Phone',
            'Created at',
            'Updated at'
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'A1:W1'; // All headers
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(14);
            },
        ];
    }
}
